/**
 * Caribou HAL class implementation
 */

#include <functional>
#include <unordered_map>

#include "HAL.hpp"

#include "carboard/Carboard.hpp"
#include "falconboard/Falconboard.hpp"

using namespace caribou;

bool caribouHAL::generalResetDone = false;

caribouHAL::~caribouHAL() {
  this->Finalize();
}

void caribouHAL::Initialize() {
  if(!caribou::caribouHAL::generalResetDone) { // board needs to be reset
    generalReset();
  }
}

void caribouHAL::Finalize() {}

void caribouHAL::generalReset() {
  caribou::caribouHAL::generalResetDone = true;
}

void caribouHAL::writeMemory(memory_map mem, uintptr_t value) {
  writeMemory(mem, 0, value);
}

void caribouHAL::writeMemory(memory_map mem, size_t offset, uintptr_t value) {
  iface_mem& imem = InterfaceManager::getInterface<iface_mem>(iface_mem::configuration_type(MEM_PATH, mem));
  imem.write(std::make_pair(offset, value));
}

uintptr_t caribouHAL::readMemory(memory_map mem) {
  return readMemory(mem, 0);
}

uintptr_t caribouHAL::readMemory(memory_map mem, size_t offset) {
  iface_mem& imem = InterfaceManager::getInterface<iface_mem>(iface_mem::configuration_type(MEM_PATH, mem));

  return imem.read(offset, 1).front();
}
